# Rockyou 2021 reb00t

Original Post: https://raidforums.com/Thread-82-billion-rockyou2021-passward-list-dictionary

Download here: https://mega.nz/folder/W4pghBLb#YxJ6qynIUY9hqGHQYxSQqg

# ⚠ Warning ⚠

As the original poster, all passwords are 6-20 characters long, and all lines containing whitespaces or tabs or non-ASCII characters were removed, but my resulting unique entries were 4.2 billion.

# Description

This combines all the following passwords lists:

* https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm
* https://www.hack3r.com/forum-topic/wikipedia-wordlist
* https://github.com/danielmiessler/SecLists/tree/master/Passwords
* https://github.com/berzerk0/Probable-Wordlists
* https://weakpass.com/download

## Installation

```bash
git clone https://gitlab.com/5amu/rockyou2021
make prepare
sudo make install
```
